export class UpdateCoachProfileDto {
    name?: string;
  
    phone?: string;
  
    about?: string;
  
    price?: string;
  
    place?: string;
  
    schedule?: string;
  }
  