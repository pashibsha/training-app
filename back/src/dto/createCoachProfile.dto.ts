export class CreateCoachProfileDto {
  name: string;

  phone: string;

  about: string;

  price: string;

  place: string;

  schedule: string;
}
