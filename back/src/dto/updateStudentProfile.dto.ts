export class UpdateStudentProfileDto {
  name: string;

  phone: string;

  about: string;

  purpose: string;

  level: string;
}
