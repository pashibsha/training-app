export class CreateStudentProfileDto {
    name: string;
  
    phone: string;
  
    about: string;
  
    purpose: string;
  
    level: string;
  }
  