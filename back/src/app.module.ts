import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthController } from './controllers/auth.controller';
import { CoachProfilesController } from './controllers/coachProfile.controller';
import { StudentProfilesController } from './controllers/studentProfile.controller';

import { AuthModule } from './modules/auth.module';
import { UserModule } from './modules/user.module';
import { ProfilesModule } from './modules/profile.module';

import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { CoachProfilesService } from './services/coachProfile.service';
import { StudentProfilesService } from './services/studentProfile.service';

import { User } from './entities/user.entity';
import { CoachProfile } from './entities/coachProfile.entity';
import { StudentProfile } from './entities/studentProfile.entity';

import { configService } from './config/config.service';

@Module({
  imports: [
    // TypeOrmModule.forRoot({
    //   type: 'postgres',
    //   host: 'localhost',
    //   port: 5433,
    //   username: 'postgres',
    //   password: 'root',
    //   database: 'training-app',
    //   entities: [User, Token],
    //   synchronize: true,
    // }),
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    TypeOrmModule.forFeature([User, CoachProfile, StudentProfile]),
    AuthModule,
    UserModule,
    ProfilesModule,
  ],
  controllers: [
    AuthController,
    CoachProfilesController,
    StudentProfilesController,
  ],
  providers: [
    UserService,
    AuthService,
    CoachProfilesService,
    StudentProfilesService,
  ],
})
export class AppModule {}
