import * as uuid from 'uuid'

export const validateUuid = (value: string): boolean => {
  return uuid.validate(value)
}
