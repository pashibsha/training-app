import { Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt'

import { AuthService } from '../services/auth.service'

import { Payload } from '../types/payload.interface'

import { ApiError } from '../exceptions/ApiError.exception'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: '' + process.env.ACCESS_SECRET_KEY,
    })
  }

  async validate(payload: Payload, done: VerifiedCallback) {
    const user = await this.authService.validateUser(payload)
    if (!user) {
      return done(ApiError.ForbiddenError(), false)
    }

    return done(null, user, payload.iat)
  }
}
