import { Body, Controller, Get, Post, Put, UseGuards } from '@nestjs/common';

import { StudentProfilesService } from '../services/studentProfile.service';

import { CreateStudentProfileDto } from '../dto/createStudentProfile.dto';
import { UpdateStudentProfileDto } from '../dto/updateStudentProfile.dto';

import { StudentProfile } from '../entities/studentProfile.entity';

@Controller('profiles')
export class StudentProfilesController {
  constructor(private readonly profileService: StudentProfilesService) {}

  @Post()
  async create(@Body() createProfile: CreateStudentProfileDto): Promise<StudentProfile> {
    return this.profileService.create(createProfile);
  }

  @Get()
  async getAll(): Promise<StudentProfile[]> {
    return this.profileService.findAll();
  }

  @Put()
  async update(@Body() updateProfile: UpdateStudentProfileDto) {
    return this.profileService.updateRole(updateProfile);
  }
}
