import { Body, Controller, Get, Post, Put, UseGuards } from '@nestjs/common';

import { CoachProfilesService } from '../services/coachProfile.service';

import { CreateCoachProfileDto } from '../dto/createCoachProfile.dto';
import { UpdateCoachProfileDto } from '../dto/updateCoachProfile.dto';

import { CoachProfile } from '../entities/coachProfile.entity';

@Controller('coach-profiles')
export class CoachProfilesController {
  constructor(private readonly profileService: CoachProfilesService) {}

  @Post()
  async create(@Body() createProfile: CreateCoachProfileDto): Promise<CoachProfile> {
    return this.profileService.create(createProfile);
  }

  @Get()
  async getAll(): Promise<CoachProfile[]> {
    return this.profileService.findAll();
  }

  @Put()
  async update(@Body() updateProfile: UpdateCoachProfileDto) {
    return this.profileService.updateRole(updateProfile);
  }
}
