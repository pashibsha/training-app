import { Body, Controller, Post } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'

import { UserService } from '../services/user.service'
import { AuthService } from '../services/auth.service'

import { LoginDto } from '../dto/login.dto'
import { RefreshDto } from '../dto/refresh.dto'
import { RegisterDto } from '../dto/register.dto'

import { ApiError } from '../exceptions/ApiError.exception'
import { ErrorMessage } from '../exceptions/errorMessage'

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('register')
  async register(@Body() registerDto: RegisterDto) {
    const findUser = await this.userService.findByEmail(registerDto.email)

    if (findUser) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['email'])
    }

    const user = await this.userService.register(registerDto)

    const token = await this.authService.signPayload(user)

    await this.userService.saveToken(
      user.id,
      token.accessToken,
      token.refreshToken,
    )

    return {
      user,
      accessToken: token.accessToken,
      refreshToken: token.refreshToken,
    }
  }

  @Post('login')
  async login(@Body() loginDto: LoginDto) {
    const user = await this.userService.findByEmail(loginDto.email)

    if (!user) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['email'])
    }

    const checkPassword = await this.userService.checkPassword(loginDto)

    if (!checkPassword) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['password'])
    }

    const token = await this.authService.signPayload(user)

    await this.userService.saveToken(
      user.id,
      token.accessToken,
      token.refreshToken,
    )

    return {
      user,
      accessToken: token.accessToken,
      refreshToken: token.refreshToken,
    }
  }

  // @Post('refresh')
  // async refresh(@Body() refreshDto: RefreshDto) {
  //   const { refreshToken } = refreshDto

  //   const user = await this.userService.validateUser(refreshToken)
  //   if (!user) {
  //     throw ApiError.UnauthorizedError()
  //   }

  //   const token = await this.authService.signPayload(user)

  //   await this.userService.saveToken(
  //     user.id,
  //     token.accessToken,
  //     token.refreshToken,
  //   )

  //   return {
  //     user,
  //     accessToken: token.accessToken,
  //     refreshToken: token.refreshToken,
  //   }
  // }

  @Post('logout')
  async logout(@Body() refreshDto: RefreshDto) {
    const { refreshToken } = refreshDto

    const user = await this.userService.validateUser(refreshToken)
    if (!user) {
      throw ApiError.BadRequest(ErrorMessage.VALIDATION_ERROR, ['refreshToken'])
    }

    await this.userService.deleteToken(user.id)

    return true
  }
}
