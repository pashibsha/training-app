export interface Payload {
  id: string
  email: string
  refreshToken?: string
  iat?: number
  exp?: number
}
