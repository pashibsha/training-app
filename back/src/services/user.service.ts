import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as moment from 'moment';
import * as bcrypt from 'bcrypt';

import { User as UserEntity } from '../entities/user.entity';
import { Token as TokenEntity } from '../entities/token.entity';

import { User } from '../types/user.interface';
import { Payload } from '../types/payload.interface';

import { validateUuid } from '../functions/uuid';

import { RegisterDto } from '../dto/register.dto';
import { LoginDto } from '../dto/login.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(TokenEntity)
    private readonly tokenRepository: Repository<TokenEntity>,
  ) {}

  async register(registerDto: RegisterDto): Promise<User> {
    const { password } = registerDto

    const passwordHashed = await this.passwordHashed(password)

    const createdUser = this.userRepository.create({
      ...registerDto,
      password: passwordHashed,
    })

    await this.userRepository.save(createdUser)

    const output = await this.findById(createdUser.id)

    return output
  }

  async findById(id: string): Promise<User | null> {
    if (!validateUuid(id)) {
      return null;
    }

    const user = await this.userRepository.findOne({
      where: { id },
      select: { id: true, email: true },
    });

    return user ? user : null;
  }

  async findByEmail(email: string): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { email },
      select: { id: true, email: true },
    });

    return user;
  }

  async findByPayload(payload: Payload): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { id: payload.id, email: payload.email },
      select: { id: true, email: true }
    });

    return user;
  }

  async saveToken(id: string, accessToken: string, refreshToken: string) {
    const token = await this.tokenRepository.findOneBy({ user: { id } });

    const expiresRefresh = new Date(
      moment()
        .add(process.env.REFRESH_EXPIRES_IN || 86400000, 'ms')
        .toISOString(),
    );

    if (!token) {
      const token = this.tokenRepository.create({
        user: { id },
        accessToken,
        refreshToken,
        expiresIn: expiresRefresh,
      });
      await this.tokenRepository.save(token);
    } else {
      token.refreshToken = refreshToken;
      token.accessToken = accessToken;
      token.expiresIn = expiresRefresh;
      await this.tokenRepository.save(token);
    }

    const output = await this.findById(id);

    return output;
  }

  async deleteToken(id: string) {
    const token = await this.tokenRepository.findOne({
      where: { user: { id } },
      relations: ['user'],
    });
    token.refreshToken = null;
    token.accessToken = null;

    await this.tokenRepository.save(token);

    const output = await this.findById(token.user.id);

    return output;
  }

  async checkPassword(loginDto: LoginDto) {
    const user = await this.userRepository.findOne({
      where: {
        email: loginDto.email,
      },
    });

    return await this.passwordVerify(loginDto.password, user.password);
  }

  async passwordHashed(password: string): Promise<string> {
    return await bcrypt.hash(password, 10);
  }

  async passwordVerify(password: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(password, hash);
  }

  async validateUser(refreshToken: string): Promise<User | null> {
    const user = await this.tokenRepository.findOne({
      where: { refreshToken },
      relations: ['user'],
      select: {
        user: { id: true, email: true },
      },
    });

    if (!user?.expiresIn) {
      return null;
    }

    const isExpires = moment(new Date().toISOString()).isBefore(user.expiresIn);

    return isExpires ? user.user : null;
  }
}
