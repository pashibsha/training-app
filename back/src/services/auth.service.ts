import { Injectable } from '@nestjs/common'
import { HmacSHA256 } from 'crypto-js'
import { sign, verify } from 'jsonwebtoken'

import { UserService } from './user.service'

import { Payload } from '../types/payload.interface'
import { Tokens } from '../types/tokens.interface'
import { User } from '../types/user.interface'

import { ApiError } from '../exceptions/ApiError.exception'

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService) {}

  async signPayload(user: User): Promise<Tokens> {
    const payload = {
      id: user.id,
      email: user.email,
    }

    const accessToken = sign(payload, process.env.ACCESS_SECRET_KEY, {
      expiresIn: process.env.ACCESS_EXPIRES_IN || 900000,
    })

    const refreshToken = HmacSHA256(
      new Date().getTime().toString(),
      process.env.REFRESH_SECRET_KEY,
    ).toString()

    return { accessToken, refreshToken }
  }

  verifyAccessToken(accessToken: string): any {
    try {
      return verify(accessToken, process.env.ACCESS_SECRET_KEY)
    } catch (error) {
      throw ApiError.UnauthorizedError()
    }
  }

  async validateUser(payload: Payload): Promise<User> {
    return await this.userService.findByPayload(payload)
  }
}
