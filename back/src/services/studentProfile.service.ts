import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { StudentProfile } from '../entities/studentProfile.entity';

import { CreateStudentProfileDto } from '../dto/createStudentProfile.dto';
import { UpdateStudentProfileDto } from '../dto/updateStudentProfile.dto';

@Injectable()
export class StudentProfilesService {
  constructor(
    @InjectRepository(StudentProfile)
    private profileRepository: Repository<StudentProfile>,
  ) {}

  async create(createProfile: CreateStudentProfileDto): Promise<StudentProfile> {
    return await this.profileRepository.save(
      this.profileRepository.create(createProfile),
    );
  }

  async findAll(): Promise<StudentProfile[]> {
    return await this.profileRepository.find();
  }

  async updateRole(updateProfile: UpdateStudentProfileDto) {
    return await this.profileRepository.save(updateProfile);
  }
}
