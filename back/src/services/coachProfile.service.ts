import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CoachProfile } from '../entities/coachProfile.entity';

import { CreateCoachProfileDto } from '../dto/createCoachProfile.dto';
import { UpdateCoachProfileDto } from '../dto/updateCoachProfile.dto';

@Injectable()
export class CoachProfilesService {
  constructor(
    @InjectRepository(CoachProfile)
    private profileRepository: Repository<CoachProfile>,
  ) {}

  async create(createProfile: CreateCoachProfileDto): Promise<CoachProfile> {
    return await this.profileRepository.save(
      this.profileRepository.create(createProfile),
    );
  }

  async findAll(): Promise<CoachProfile[]> {
    return await this.profileRepository.find();
  }

  async updateRole(updateProfile: UpdateCoachProfileDto) {
    return await this.profileRepository.save(updateProfile);
  }
}
