import {
    Column,
    Entity,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
  } from 'typeorm'
  
  import { User } from './user.entity'
  
  @Entity({ name: 'tokens' })
  export class Token {
    @PrimaryGeneratedColumn('uuid')
    public id: string
  
    @OneToOne(() => User, (user) => user.id)
    @JoinColumn({ name: 'user_id' })
    public user: User
  
    @Column({ nullable: true, name: 'access_token' })
    public accessToken: string | null
  
    @Column({ nullable: true, name: 'refresh_token' })
    public refreshToken: string | null
  
    @Column({ type: 'timestamptz', nullable: true })
    public expiresIn: Date | null
  }
  