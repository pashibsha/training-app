import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'coach-profiles' })
export class CoachProfile {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column()
  about: string;

  @Column()
  price: string;

  @Column()
  place: string;

  @Column()
  schedule: string;
}
