import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'student-profiles' })
export class StudentProfile {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column()
  about: string;

  @Column()
  purpose: string;

  @Column()
  level: string;
}
