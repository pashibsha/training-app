import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn('uuid')
  public id: string

  @Column()
  public email: string

  @Column()
  public password: string

  @Column()
  public role: string
}
