import { Module } from '@nestjs/common';

import { AuthController } from '../controllers/auth.controller';
import { UserModule } from '../modules/user.module';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { JwtStrategy } from '../functions/jwt.strategy';

@Module({
  imports: [UserModule],
  controllers: [AuthController],
  providers: [AuthService, UserService, JwtStrategy],
})
export class AuthModule {}
