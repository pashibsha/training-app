import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";

import { CoachProfilesController } from "../controllers/coachProfile.controller";
import { StudentProfilesController } from "../controllers/studentProfile.controller";

import { CoachProfile } from "../entities/coachProfile.entity";
import { StudentProfile } from "../entities/studentProfile.entity";

import { CoachProfilesService } from "../services/coachProfile.service";
import { StudentProfilesService } from "../services/studentProfile.service";

@Module({
  imports: [TypeOrmModule.forFeature([CoachProfile, StudentProfile])],
  controllers: [CoachProfilesController, StudentProfilesController],
  providers: [CoachProfilesService, StudentProfilesService],
  exports: [CoachProfilesService, StudentProfilesService],
})
export class ProfilesModule {}