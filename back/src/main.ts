import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { AppModule } from './app.module'
import { configService } from './config/config.service'

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule)

  app.enableCors()

  if (!configService.isProduction()) {
    const options = new DocumentBuilder()
      .setTitle('Training App')
      .setDescription('API')
      .setVersion('1.0')
      .addBearerAuth()
      .build()

    const document = SwaggerModule.createDocument(app, options)
    SwaggerModule.setup('docs', app, document)
  }

  await app.listen(process.env.PORT)
}

bootstrap()
