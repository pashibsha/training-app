import { HttpException, HttpStatus } from '@nestjs/common'
import { ErrorMessage } from './errorMessage'

export class ApiError extends HttpException {
  constructor(status: HttpStatus, message: ErrorMessage, props = {}) {
    super({ message, props }, status)
  }

  /**
   * Ошибка 401 - Пользователь не авторизирован
   * @static
   */
  static UnauthorizedError(): ApiError {
    return new ApiError(HttpStatus.UNAUTHORIZED, ErrorMessage.NOT_AUTHORIZED)
  }

  /**
   * Ошибка 403 - Доступ запрещен
   * @static
   */
  static ForbiddenError() {
    return new ApiError(HttpStatus.FORBIDDEN, ErrorMessage.FORBIDDEN_ERROR)
  }

  /**
   * Ошибка 400 - Плохой запрос.
   * Если пользователь указал какие-то некорректные параметры или не прошел валидацию
   * @static
   */
  static BadRequest(message: ErrorMessage, props?: any[]) {
    if (message) {
      return new ApiError(HttpStatus.BAD_REQUEST, message, props)
    }

    return new ApiError(
      HttpStatus.BAD_REQUEST,
      ErrorMessage.INVALID_REQUEST,
      props,
    )
  }

  /**
   * Ошибка 500 - Ошибка сервера
   * @static
   */
  static InternalError(message?: ErrorMessage) {
    if (message) {
      return new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, message)
    }

    return new ApiError(
      HttpStatus.INTERNAL_SERVER_ERROR,
      ErrorMessage.UNEXPECTED_ERROR,
    )
  }
}
