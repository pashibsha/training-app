import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

import Messanger from './views/messanger'
import CreateProfileStudent from './views/create-profile-student'
import Register from './views/register'
import Messages from './views/messages'
import Home from './views/home'
import Page from './views/page'
import CreateProfileCoach from './views/create-profile-coach'
import Slider2 from './views/slider2'
import Login from './views/login'
import Slider from './views/slider'
import NotFound from './views/not-found'
import './style.css'

Vue.use(Router)
Vue.use(Meta)
export default new Router({
  mode: 'history',
  routes: [
    {
      name: 'messanger',
      path: '/messanger',
      component: Messanger,
    },
    {
      name: 'Create-profile-student',
      path: '/create-profile-student',
      component: CreateProfileStudent,
    },
    {
      name: 'Register',
      path: '/register',
      component: Register,
    },
    {
      name: 'messages',
      path: '/messages',
      component: Messages,
    },
    {
      name: 'Home',
      path: '/',
      component: Home,
    },
    {
      name: 'Page',
      path: '/page',
      component: Page,
    },
    {
      name: 'Create-profile-coach',
      path: '/create-profile-coach',
      component: CreateProfileCoach,
    },
    {
      name: 'Slider2',
      path: '/slider2',
      component: Slider2,
    },
    {
      name: 'Login',
      path: '/login',
      component: Login,
    },
    {
      name: 'Slider',
      path: '/slider',
      component: Slider,
    },
    {
      name: '404 - Not Found',
      path: '**',
      component: NotFound,
      fallback: true,
    },
  ],
})
